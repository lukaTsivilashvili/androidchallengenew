package retrofitStuff

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.liveData
import com.example.appchallengenew.InfoModel
import com.example.appchallengenew.InfoPagingSource

class InfoRepo {

    companion object {
        private const val NETWORK_PAGE_SIZE = 10
    }

    fun getNews(): LiveData<PagingData<InfoModel>> {
        Log.d("PixaBayRepository", "New page")
        return Pager(
            config = PagingConfig(
                pageSize = NETWORK_PAGE_SIZE,
                enablePlaceholders = true
            ),
            pagingSourceFactory = { InfoPagingSource() }
        ).liveData
    }

}