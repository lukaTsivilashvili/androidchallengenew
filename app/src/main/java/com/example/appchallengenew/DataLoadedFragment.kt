package com.example.appchallengenew

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AbsListView
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appchallengenew.databinding.DataLoadedFragmentBinding
import com.google.gson.Gson
import interfaces.OnLoadMoreListener
import kotlinx.coroutines.launch

class DataLoadedFragment : Fragment() {

    private val viewModel: DataLoadedViewModel by viewModels()
    private lateinit var binding: DataLoadedFragmentBinding
    private lateinit var adapter: NewsRecyclerViewAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataLoadedFragmentBinding.inflate(
            inflater, container, false
        )
        init()
        return binding.root
    }

    private fun init() {
        initRecyclerView()
        fetchNews()
    }

    private fun fetchNews() {
        viewModel.fetchNews().observe(viewLifecycleOwner, {
            lifecycleScope.launch {
                adapter.submitData(it)
            }
        })
    }

    private fun initRecyclerView() {
        binding.recyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        adapter = NewsRecyclerViewAdapter()
        binding.recyclerView.adapter = adapter

    }
}
