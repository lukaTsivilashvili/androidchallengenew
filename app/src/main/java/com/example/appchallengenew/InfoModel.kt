package com.example.appchallengenew

data class InfoModel (
    val id: Int = 0,
    val titleKA: String = "",
    val cover: String = "",
    var isLast:Boolean = false
    )
