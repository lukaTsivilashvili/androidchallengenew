package com.example.appchallengenew

sealed class Result<T>{

    data class Success<T>(val data:T):Result<T>()
    data class Error<T>(val eMessage:String):Result<T>()
    data class Loading<T>(val load:Boolean):Result<T>()

}
