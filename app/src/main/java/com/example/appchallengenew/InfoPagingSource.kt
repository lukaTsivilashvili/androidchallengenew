package com.example.appchallengenew


import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState
import retrofit2.HttpException
import retrofitStuff.RetrofitService
import java.io.IOException

private const val STARTING_PAGE_INDEX = 1

class InfoPagingSource: PagingSource<Int,InfoModel >() {
    override fun getRefreshKey(state: PagingState<Int, InfoModel>): Int? {
        TODO("Not yet implemented")
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, InfoModel> {
        val position = params.key ?: STARTING_PAGE_INDEX
        return try {
            val response = RetrofitService.retrofit().getRequest(ApiMethod.news,position)
            val data = response.body()!!
            LoadResult.Page(
                data = data,
                prevKey = if (position == STARTING_PAGE_INDEX) null else position,
                nextKey = if (data.isEmpty()) null else position + 1
            )
        } catch (exception: IOException) {
            return LoadResult.Error(exception)
        } catch (exception: HttpException) {
            return LoadResult.Error(exception)
        }
    }
}
