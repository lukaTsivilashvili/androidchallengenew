package com.example.appchallengenew

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.appchallengenew.databinding.ItemsLayoutBinding

class NewsRecyclerViewAdapter() : PagingDataAdapter<InfoModel, RecyclerView.ViewHolder>(REPO_COMPARATOR) {


    companion object {
        private val REPO_COMPARATOR = object : DiffUtil.ItemCallback<InfoModel>() {
            override fun areItemsTheSame(oldItem: InfoModel, newItem: InfoModel): Boolean =
                oldItem == newItem

            override fun areContentsTheSame(oldItem: InfoModel, newItem: InfoModel): Boolean =
                oldItem.id == newItem.id
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return WallPostsViewHolder(
            ItemsLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as? WallPostsViewHolder)?.onBind()

    }

    inner class WallPostsViewHolder(private val binding: ItemsLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private lateinit var model: InfoModel

        fun onBind() {
            model = getItem(absoluteAdapterPosition)!!
            Glide.with(App.instance.getContext()).load(model.cover)
                .into(binding.newsCoverImageView)
            binding.titleTextView.text = model.titleKA
        }
    }
}
