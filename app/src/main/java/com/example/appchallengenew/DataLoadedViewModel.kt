package com.example.appchallengenew

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.university.exam8.ui.tools.Tools
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response
import retrofitStuff.InfoRepo
import retrofitStuff.RetrofitService
import javax.sql.DataSource

class DataLoadedViewModel : ViewModel() {

    private val retroRepo = InfoRepo()

    fun fetchNews(): LiveData<PagingData<InfoModel>> {
        return retroRepo.getNews().cachedIn(viewModelScope)
    }
}
