package interfaces

interface OnLoadMoreListener {
    fun onLoadMore()
}